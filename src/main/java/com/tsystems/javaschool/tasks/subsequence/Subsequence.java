package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    
 
    public boolean find(List x, List y) {
    	   if (x == null || y == null){
    			throw new IllegalArgumentException();
    		}
    		if (x.size() <= y.size()){
    			int i = 0;
    			int j = 0;
    			while(i < x.size()){
    				Object aim = x.get(i);
    				if (j < y.size()){
    					while (j < y.size() && !y.get(j).equals(aim)){
    						j++;
    					}
    					j++;
    					i++;
    				}else{
    					break;
    				}
    			}
    			return (i == x.size() && j <= y.size());
    		}
    		return false;
    }
}
